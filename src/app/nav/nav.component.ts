import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public loggedIn:boolean=false;

  constructor(private Auth:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.Auth.authStatus.subscribe(
      value => {
        this.loggedIn = value
      }
    )
  }
  logout(event:MouseEvent){
    event.preventDefault()
    this.router.navigateByUrl('/login');
    this.Auth.removeToken()
    this.Auth.changeAuthStatus(false)
  }

}
