import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductsComponent} from "./products/products.component";
import {OrdersComponent} from "./orders/orders.component";
import {RegisterComponent} from "./register/register.component";
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./guards/auth.guard";
import {NotauthGuard} from "./guards/notauth.guard";
import {CreateOrderComponent} from "./orders/create-order/create-order.component";

//TODO:add root for product edit
//FIXME:Just teste

const routes: Routes = [
  {

    path:'',
    redirectTo:'/login',
    pathMatch:'full',
  },
  {
    path:'orders/create-order',
    component:CreateOrderComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'products',
    component:ProductsComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'orders',
    component:OrdersComponent,
    canActivate:[AuthGuard],
  },
  {
    path:'login',
    component:LoginComponent,
    canActivate:[NotauthGuard]

  },
  {
    path:'register',
    component:RegisterComponent,
    canActivate:[NotauthGuard]
  },
    {
    path:'home',
    component:HomeComponent,
    canActivate:[AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
