import { Injectable } from '@angular/core';
import { CanActivate,Router } from '@angular/router';
import {AuthService} from "../services/auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class NotauthGuard implements CanActivate {
  constructor(private authService:AuthService,private router:Router) {
  }
  canActivate():boolean{
    if(this.authService.loggedIn()){
      this.authService.changeAuthStatus(true)
      this.router.navigate(['/home']);
      return false
    }else{
      return true
    }
  }

}
