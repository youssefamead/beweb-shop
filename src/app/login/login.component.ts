import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";

//TODO:Typescript error problems

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserData={
    email: undefined,
    password: undefined,
  }
  errorMsg:any

  constructor(private _auth:AuthService,private _router:Router) { }

  ngOnInit(): void {
  }

  loginUser() {

    this._auth.loginUser(this.loginUserData)
      .subscribe(
        res =>{
          localStorage.setItem('token',res.token)
          this._router.navigate(['/home'])
        },
        err=>this.errorMsg=err.error.message
      );
  }


}
