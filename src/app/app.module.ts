import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { OrdersComponent } from './orders/orders.component';
import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule,HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthService} from "./services/auth/auth.service";
import {ProductService} from "./services/product/product.service";
import { HomeComponent } from './home/home.component';
import {AuthGuard} from "./guards/auth.guard";
import {TokenInterceptorService} from "./services/token-interceptor/token-interceptor.service";
import {NotauthGuard} from "./guards/notauth.guard";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from "@angular/forms";
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {TableModule} from "primeng/table";
import { CommonModule } from '@angular/common';
import { AddProductComponent } from './products/add-product/add-product.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import { ToastrModule } from 'ngx-toastr';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { CreateOrderComponent } from './orders/create-order/create-order.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatStepperModule} from '@angular/material/stepper';
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {InputTextModule} from "primeng/inputtext";


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    ProductsComponent,
    OrdersComponent,
    NavComponent,
    LoginComponent,
    HomeComponent,
    AddProductComponent,
    EditProductComponent,
    OrderDetailsComponent,
    CreateOrderComponent,
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatDialogModule,
        ReactiveFormsModule,
        AccordionModule,
        TableModule,
        CommonModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatTooltipModule,
        ToastrModule.forRoot(),
        MatStepperModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        InputTextModule
    ],
  providers: [
    AuthService,
    ProductService,
    AuthGuard,
    NotauthGuard,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:TokenInterceptorService,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
