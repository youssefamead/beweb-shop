import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {ProductService} from "../../services/product/product.service";
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})


//FIXME:rename Class Product to app-product-add

export class AddProductComponent implements OnInit {

  @Output("productsRefresh") productsRefresh: EventEmitter<any> = new EventEmitter();
  productForm!:FormGroup

  @Input() isAddMode:any
  @Input() productToEdit:any

  constructor(private formBuilder:FormBuilder,
              private _productService :ProductService,
              private toastr: ToastrService

  ) { }

  ngOnInit(): void {
    this.productForm=this.formBuilder.group({
      name:['',Validators.required],
      price:['',Validators.required]
    })

  }


  // Add new Product and show success or error notification
  addProduct(){
    this.isAddMode=true
    console.log(this.productForm)
    if(!this.productForm.valid){
      this.toastr.error(" Error!", "Product Creatin has been failed  !!")
      return ;
    }

    this._productService.createProduct(this.productForm.value).subscribe(
      req =>{
        // console.log(req)
        this.productForm.reset();
        this.productsRefresh.emit();
        this.toastr.success(" Success!", "Product Created successfully !!")
      },

      error => {
        console.log(error)
        this.toastr.error(" Error!", "Product Creatin has been failed  !!")
      }
    )
  }


  get f()
  {
    return this.productForm.controls;
  }

}
