import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../services/product/product.service';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  //FIXME : change loading name to isLoading

  products = [];
  totalRecords: any;
  loading: boolean = false;
  isAddMode = true;
  productToEdit: object = {};

  constructor(private _productService: ProductService) {}

  ngOnInit(): void {
    this.getProducts();
  }

  addProduct() {
    this.isAddMode = true;
    this.openDialog();
  }

  openDialog() {
    $('#addProductModal').modal('show');
  }

  getProducts() {
    this._productService.getProducts().subscribe(
      (res) => {
        this.products = res.data;
        this.totalRecords = this.products.length;
        this.loading = true;
      },
      (err) => console.log(err)
    );
  }

  editProduct(id: any) {
    this._productService.getProductById(id).subscribe(
      (res) => {
        this.productToEdit = res.data;
      },
      (err) => console.log(err)
    );
    this.isAddMode = false;
    this.openDialog();
  }

  deleteProduct(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this._productService.deleteProduct(id).subscribe(
          (res) => {
            Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
            this.getProducts();
          },
          (err) => console.log(err)
        );
      }
    });
  }
}
