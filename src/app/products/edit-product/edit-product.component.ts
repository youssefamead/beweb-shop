import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../../services/product/product.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Output("productsRefresh") productsRefresh: EventEmitter<any> = new EventEmitter();
  @Input() productToEdit:any
  productEditForm!:FormGroup

  constructor(private formBuilder:FormBuilder,
              private _productService :ProductService,
              private toastr: ToastrService) { }

  ngOnInit(): void {

    this.productEditForm=this.formBuilder.group({
      name:['',Validators.required],
      price:['',Validators.required]
    })

  }
  updateProduct(){

    //FIXME: verify if form is valid

    this._productService.updateProduct(this.productToEdit.id,this.productEditForm.value).subscribe(
      req =>{
        // console.log(req)
        this.productsRefresh.emit();
        this.toastr.success(" Success!", "Product Updated successfully !!")
      },
      err =>{
        // console.log(err)
        this.toastr.error(" Error!", "Product Updating has been failed  !!")
      }
    )

  }

}
