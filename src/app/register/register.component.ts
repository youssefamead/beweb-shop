import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth/auth.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerUserData={
    name:undefined,
    password:undefined,
    email:undefined,
    password_confirmation:undefined
  }
  errorMsg:any

  constructor(private _auth:AuthService,private _router:Router) { }

  ngOnInit(): void {
  }
  registerUser(){
    this._auth.registerUser(this.registerUserData)
      .subscribe(
        res =>{
          localStorage.setItem('token',res.token)
          this._router.navigate(['/home'])
        },
             err=>this.errorMsg='Registration Failed , Please Try again'
      );
  }

}
