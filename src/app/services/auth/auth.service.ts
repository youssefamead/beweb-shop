import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {catchError} from "rxjs/operators";
import {throwError} from 'rxjs';
import {environment} from '@environments/environment'

//TODO:Put URL in envirement File
//FIXME:Delete UnderScore

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private loggedInU= new BehaviorSubject<boolean>(this.loggedIn())
  private _registerUrl = environment.apiUrl+"/register"
  private _loginUrl = environment.apiUrl+ "/login"

  authStatus= this.loggedInU.asObservable();


  constructor(private http:HttpClient) { }

  registerUser(user:any){

    return this.http.post<any>(this._registerUrl,user)
      .pipe(catchError(this.errorHandler))

  }

  loginUser(user:any){

    return this.http.post<any>(this._loginUrl,user)

  }

  loggedIn(){
    //return this.isTokenValid()
    return !!localStorage.getItem('token')
  }

  getToken(){

    return localStorage.getItem('token')
  }
  removeToken(){
    return localStorage.removeItem('token')
  }

  isTokenValid(){

    const token = this.getToken();
    if(token){
     const payload = this.payLoad(token)
      if(payload!==null){
        return (payload.iss==="http://127.0.0.1:8000/api/login")?true:false
      }
    }
    return false
  }

  payLoad(token:any){
    const payload = token.split(',')[1]
    return this.decodeToken(payload)
  }
  decodeToken(payload :any){
    return JSON.parse(payload)

  }
  changeAuthStatus(value:boolean){
    this.loggedInU.next(value)
  }
  errorHandler(error : HttpErrorResponse){
    return throwError(error)
  }

}
