import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";


//FIXME:Envirement + Underscore

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private _ordersUrl="http://127.0.0.1:8000/api/order"
  private _makeOrderUrl="http://127.0.0.1:8000/api/make_order"

  constructor(private http:HttpClient) { }

  getOrders(){
    return this.http.get<any>(this._ordersUrl)
  }
  makeOrder(order:object){
    return  this.http.post<any>(this._makeOrderUrl,order);
  }

}
