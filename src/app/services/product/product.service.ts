
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})

//FIXME: envirement file + delete Underscore
export class ProductService {

  private _productsUrl="http://127.0.0.1:8000/api/product/"

  constructor(private http:HttpClient) { }

  getProducts(){

    return this.http.get<any>(this._productsUrl)

  }

  createProduct(product:object){

   return  this.http.post<any>(this._productsUrl,product);

  }

  getProductById(id:any){

    return this.http.get<any>(this._productsUrl+id);

  }

  updateProduct(id:any ,product:object){

    return  this.http.put<any>(this._productsUrl+id,product);

  }

  deleteProduct(id:any){

    return  this.http.delete<any>(this._productsUrl+id);
  }

}
