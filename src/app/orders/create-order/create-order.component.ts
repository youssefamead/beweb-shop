import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../services/product/product.service';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from '../../services/order/order.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css'],
})
export class CreateOrderComponent implements OnInit {
  isValidOrder = false;

  savedOrder = {
    first_name: undefined,
    last_name: undefined,
    order_products: [],
  };

  products = [];
  customerFormGroup!: FormGroup;
  isOptional = false;
  totalRecords: any;
  loading = false;
  selectedProducts = [];
  amount = 0;

  constructor(
    private _formBuilder: FormBuilder,
    private _productService: ProductService,
    private _orderSevice: OrderService,
    private toastr: ToastrService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.getProducts();

    this.customerFormGroup = this._formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
    });
    console.log(this.customerFormGroup.value);
  }

  getProducts() {
    this._productService.getProducts().subscribe(
      (res) => {
        this.products = res.data;
        this.totalRecords = this.products.length;
        this.loading = true;
      },
      (err) => console.log(err)
    );
  }

  selectedProduct(product: any) {
    let settedQuantity = $('#quantityField-' + product.id).val();
    this.selectedProducts.push({
      // @ts-ignore
      product_id: product.id,
      // @ts-ignore
      quantity: settedQuantity,
      // @ts-ignore
      price: product.price,
    });

    console.log(this.selectedProducts);
    $('#addProductFromOrder-' + product.id).addClass('d-none');
    $('#removeProductFromOrder-' + product.id).removeClass('d-none');
    $('#removeProductFromOrder-' + product.id)
      .closest('tr')
      .addClass('bg-success');
    $('#quantityField-' + product.id).prop('disabled', true);
    this.isValidOrderStatus();
  }

  RemoveSelectedProduct(product: any) {
    this.selectedProducts.forEach((arrayItem, index) => {
      // @ts-ignore
      if (arrayItem.product_id === product.id) {
        this.selectedProducts.splice(index, 1);
      }
      this.isValidOrderStatus();
    });

    $('#addProductFromOrder-' + product.id).removeClass('d-none');
    $('#addProductFromOrder-' + product.id)
      .closest('tr')
      .removeClass('bg-success');
    $('#removeProductFromOrder-' + product.id).addClass('d-none');
    $('#quantityField-' + product.id).prop('disabled', false);
  }

  isValidOrderStatus() {
    if (this.selectedProducts.length >= 1) {
      this.isValidOrder = true;
    } else {
      this.toastr.warning(
        ' Info!',
        'You have to add at least one product to be able to validate this order !!'
      );
      this.isValidOrder = false;
    }
  }

  saveOrder() {
    let firstname = this.customerFormGroup.value.first_name;
    let lastname = this.customerFormGroup.value.last_name;
    this.savedOrder = {
      first_name: firstname,
      last_name: lastname,
      order_products: this.selectedProducts,
    };
    this.calculateAmount(this.savedOrder.order_products);
  }

  confirmOrder() {
    this._orderSevice.makeOrder(this.savedOrder).subscribe(
      (req) => {
        Swal.fire({
          icon: 'success',
          title: 'Great',
          text: 'Order Created Successfully!',
        });
        this._router.navigate(['/orders']);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  calculateAmount(orders_products: any) {
    this.amount = 0;
    // @ts-ignore
    orders_products.forEach((item) => {
      this.amount += item.price * item.quantity ;
    });
    return this.amount;
  }
}
