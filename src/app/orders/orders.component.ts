import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../services/product/product.service';
import { OrderService } from '../services/order/order.service';
import * as _ from 'lodash';
declare var $: any;

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  orders: any;
  totalRecords: any;
  loading = false;
  orderDetails = [];
  products = [];
  constructor(
    private _orderService: OrderService,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.getOrders();
    this.getProducts();
  }

  getOrders() {
    this._orderService.getOrders().subscribe(
      (res) => {
        this.orders = res.data;
        this.totalRecords = this.orders.length;
        this.loading = true;
      },
      (err) => console.log(err)
    );
  }
  showModal() {
    $('#showOrderDetailsModal').modal('show');
  }
  addOrder() {}

  getOrderDetails(order: any) {
    console.log(order.orders);
    console.log(this.products);
    this.showModal();
    this.orderDetails = order;

    _.map(order.orders, (o) => {
      console.log(o);
      let product = _.find(this.products, (e: any) => {
        return (e.id == o.product_id);
      });

     o.name = product.name
     return o
    });
  }
  getProducts() {
    this.productService.getProducts().subscribe(
      (res) => {
        this.products = res.data;
      },
      (err) => console.log(err)
    );
  }
}
